import sqlite3
from weather import *

class SQLighter:

    def __init__(self, database_file):
        """Подключаемся к БД и сохраняем курсор соединения"""
        self.connection = sqlite3.connect(database_file)
        self.cursor =  self.connection.cursor()

    # методы для работы с statesTable

    def get_current_state(self, user_id):
        """Получаем состояние пользователя"""
        self.cursor.execute('''SELECT state
                                            FROM statesTable
                                            WHERE user_id = {}'''.format(user_id))

        return self.cursor.fetchall()[0][0]

    def set_state(self, user_id, value):
        """Изменяем состояние"""
        def is_exist_func():
            global is_exist
            self.cursor.execute('''SELECT * FROM statesTable WHERE user_id = {}'''.format(user_id))
            if len(self.cursor.fetchall()) == 0:
                is_exist = False
            else:
                is_exist = True 

        is_exist_func() 

        if is_exist:
            self.cursor.execute('''UPDATE statesTable
        				  SET state = {0}
        				  WHERE user_id = {1}'''.format(value, user_id))
            self.connection.commit()
        else:
            try:
        	    self.cursor.execute('''INSERT INTO statesTable (user_id, state)
        	    	VALUES ({0}, {1})'''.format(user_id, value))
        	    self.connection.commit()
            except:
                return False

    def get_all_users(self):
        """Получаем все id"""
        self.cursor.execute('''SELECT user_id FROM statesTable''')
        return self.cursor.fetchall()


    # методы для работы с notTable

    def subscribe(self, user_id, not_time, city):
        """Подписываемся на уведомления"""
        def is_exist_func():
            global is_exist2

            self.cursor.execute('''SELECT * FROM notTable WHERE user_id = {}'''.format(user_id))
            if len(self.cursor.fetchall()) == 0:
                is_exist2 = False
            else:
                is_exist2 = True

        is_exist_func() 

        if is_exist2:
            self.cursor.execute('''UPDATE notTable
        				           SET is_sub = 1,
                                   city = '{0}',
                                   not_time = '{1}'
        				           WHERE user_id = {2}'''.format(city, not_time, user_id))
            self.connection.commit()
        else:
        	self.cursor.execute('''INSERT INTO notTable (user_id, city, not_time, is_sub)
        	 	                   VALUES ({0}, "{1}", "{2}", True)'''.format(user_id, city, not_time))
        	self.connection.commit()

    def unsubscribe(self, user_id):
        """Отписываемся от уведомлений"""
        self.cursor.execute('''UPDATE notTable
                               SET city = Null,
                               is_sub = 0,
                               not_time = Null
                               WHERE user_id = {}'''.format(user_id))
        self.connection.commit()

    def get_sub_users(self):
        """Получение подписанных пользователей"""
        self.cursor.execute('''SELECT user_id
                               FROM notTable
                               WHERE is_sub=1''')
        return self.cursor.fetchall()

    def get_city(self, user_id):
        """Получаем город пользователя"""
        self.cursor.execute('''SELECT city
                               FROM notTable
                               WHERE user_id = {}'''.format(user_id))
        return self.cursor.fetchall()[0][0]

    def get_not_time_list(self, user_id, is_sub):
        """Получение времени уведомление в формате ['5', '30']"""
        self.cursor.execute('''SELECT not_time
						  FROM notTable
						  WHERE user_id = {0} and is_sub = {1}'''.format(user_id, is_sub))
        return self.cursor.fetchall()[0][0].split(':')

    def get_sub_users_in_city(self, city):
        self.cursor.execute('''SELECT city
					  FROM notTable
					  WHERE is_sub = True and city="{}"'''.format(city))
        users_subscribed = self.cursor.fetchall()
        return users_subscribed



db = SQLighter('db.db')
print(db.get_current_state(662211080))
# print(notif_forecast(db.get_city(1365882584)))
# print(str(db.get_city(1365882584)))



# db = SQLighter('db.db')
# print(db.get_current_state(5554))
# db.subscribe(5554, '5:11', 'Москва')
# print(db.get_sub_users())
# print(db.get_city(5554))
# print(db.get_not_time_list(5554, 1))
# print(db.get_sub_users_in_city('Москва'))